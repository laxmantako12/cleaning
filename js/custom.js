jQuery(document).ready(function ($) {
    $(function () {
        AOS.init();
    });

    function custommatchHeight() {
        var options = ({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
        $('.why-choose-us__block .title h4').matchHeight(options);

    }

    custommatchHeight()

    function sliderContentHeight() {
        var options = ({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.portfolio__slider__block h3').matchHeight(options);
        $('.section-services__block .content-block h4').matchHeight(options);

    }
    sliderContentHeight();
    // $('.portfolio__slider').slick({
    //     dots: false,
    //     infinite: true,
    //     arrows: true,
    //     prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left"></i></button>',
    //     nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right"></i></button>',
    //     speed: 1000,
    //     slidesToShow: 2,
    //     slidesToScroll: 1,
    //     centerMode: true,
    //     adaptiveHeight: true,
    //     centerPadding: '60px',
    //     responsive: [{
    //             breakpoint: 1024,
    //             settings: {
    //                 slidesToShow: 2,
    //                 slidesToScroll: 1,
    //             }
    //         },
    //         {
    //             breakpoint: 600,
    //             settings: {
    //                 slidesToShow: 1,
    //                 slidesToScroll: 1
    //             }
    //         },
    //         {
    //             breakpoint: 480,
    //             settings: {
    //                 slidesToShow: 1,
    //                 slidesToScroll: 1
    //             }
    //         }
    //     ]
    // });

    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 70
        }, 2000);
    });



    $('.portfolio__slider').on('init', function (event, slick) {
        sliderContentHeight();
    });

    $('.portfolio__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        sliderContentHeight();
    });

    $('.portfolio__slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        sliderContentHeight();
    });

    $('.portfolio__slider').on('setPosition', function (event, slick) {
        sliderContentHeight();
    });
    // svg image to code
    jQuery('img.svg-icon').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
    $('.banner__slider').slick({
        autoplay: true,
        speed: 800,
        lazyLoad: 'progressive',
        arrows: true,
        dots: false,
        prevArrow: '<div class="slick-nav prev-arrow"><i></i><div class="spinner"></div></div>',
        nextArrow: '<div class="slick-nav next-arrow"><i></i><div  class="spinner"></div></div>',
    }).slickAnimation();



    $('.slick-nav').on('click touch', function (e) {

        e.preventDefault();

        var arrow = $(this);

        if (!arrow.hasClass('animate')) {
            arrow.addClass('animate');
            setTimeout(() => {
                arrow.removeClass('animate');
            }, 1600);
        }

    });


    //   nav show and hide
    $('body').on('click', '.navbar-toggler', function () {
        $(this).toggleClass('on');
        $('.navbar-collapse').slideToggle('');
    });

    $('body').on('click', '.navbar-nav li a', function () {
        $('.navbar-toggler').toggleClass('on');
        $('.navbar-collapse').slideToggle('');
    });

    function navbarWidth() {
        var navWidth = $(window).width();
        $('.navbar').css({
            'width': navWidth,
        });
    }
    navbarWidth();
    $(window).resize(navbarWidth);


    function fixHeaderSpacing() {
        var headerHeight = $('.navbar').height();
        $('body').css({
            'padding-top': headerHeight,
        });
    }
    fixHeaderSpacing();
    $(window).resize(fixHeaderSpacing);

    function containerOffset() {
        var navbarOffset = $('.navbar .container').offset().left + 15;
        $('.offset-section .main-title').css({
            'padding-left': navbarOffset,
        });
    }
    containerOffset();
    $(window).resize(containerOffset);
    // offset-section

});
